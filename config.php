<?php

/*
 * 配置文件
 * private_key_path和public_key_path或者private_key和public_key 二选一，必填，优先使用path
 */
$config = array(
    'app_id' => '',
    'notify_url' => '',
    'sign_type' => 'RSA2',
    'private_key' => '', //一行字符串，前后不要有空格和换行
    'public_key' => '', //一行字符串，前后不要有空格和换行
    'alipay_public_key' => '', //注意是支付宝公钥，用来解密支付宝返回数据签名，一行字符串，前后不要有空格和换行！！！
    'private_key_path' => '', //必须是证书所在的绝对路径
    'public_key_path' => '', //必须是证书所在的绝对路径
);
